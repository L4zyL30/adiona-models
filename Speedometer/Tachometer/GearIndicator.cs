﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class GearIndicator : MonoBehaviour {

	[SerializeField] private TextMesh gearNumber;
	[SerializeField] private CarController controller;

	void Update()
	{
		gearNumber.text = (controller.currentGear + 1).ToString();
	}
}
