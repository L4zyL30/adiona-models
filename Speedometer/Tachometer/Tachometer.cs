﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Tachometer: MonoBehaviour {

	public GameObject needle; //Identify what gameObject the needle is

	private float angle;
	private float zeroAngle;

	private CarController controller; // Get motorRPM from this

	//These variables are purely for calculatory reasons. They are explained where they are initiated
	private float minAngle;
	private float maxAngle;

	private float minRPM;
	private float maxRPM;

	private float angleDiff;
	private float speedDiff;

	private float scaledIncrement;

	private ViolationManager violations;

	void Start()
	{
		controller = transform.root.GetComponent<CarController> ();

		// No need for Tachometer when driving automatic
		if (controller.m_ShiftType != ShiftType.Manual)
			Destroy (gameObject);
		
        //55 degrees points the needle to 0. 149 degrees points to 70 (Numbers may have been changed, but the concept is the same)
        minAngle = 241;
		maxAngle = 484;
		
		//When the car hits its peak speed (in this case 70), that will determine the maxSpeed (Numbers may have been changed, but the concept is the same)
		minRPM = 0;
		maxRPM = 7000;
		
		//Takes the differences from min and max numbers
		angleDiff = maxAngle - minAngle;
		speedDiff = maxRPM - minRPM;
		
		//Scales the numbers down so that the angle and speed can be synced
		//This number means for every 1 mph that is reached, this number is used instead
		scaledIncrement = angleDiff/speedDiff;

		zeroAngle = minAngle; //Angles 55 to 149 are speed ranges from 0 - 70mph
		angle = zeroAngle; //Start position of angle
		//car = transform.parent.gameObject;
	}

	void Update ()
	{
		float motorRPM = controller.motorRPM;

		if(motorRPM <= maxRPM + 1){
			angle = (motorRPM * scaledIncrement) + minAngle;
			needle.transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
		}
	}

	/*
	NOTES:
	94 //difference for angles
	70 //difference for magnitude

	1.34 //70 goes into 94 this many times. So every mph increment must increment by 1.34 degrees

	//0 = 55
	//1 = 1.34
	//2 = 2.68
	//3 = 4.02

	*/
}