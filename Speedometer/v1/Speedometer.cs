﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class Speedometer : MonoBehaviour {

	internal enum SpeedType
	{
		MPH,
		KPH
	}

	internal enum FaceType
	{
		Analog,
		Digital
	}

	[SerializeField] private SpeedType m_SpeedType = SpeedType.MPH;
	[SerializeField] private FaceType m_FaceType = FaceType.Analog;

	public Material Digital_Face;
	public Material MPH_Face;
	public Material KPH_Face;

	public GameObject face;
	public GameObject needle; //Identify what gameObject the needle is
	public TextMesh digitalText;

	private float angle;
	private float zeroAngle;

	private Rigidbody car;
	private float carSpeed;

	//These variables are purely for calculatory reasons. They are explained where they are initiated
	private float minAngle;
	private float maxAngle;

	private float minSpeed;
	private float maxSpeed;

	private float angleDiff;
	private float speedDiff;

	private float scaledIncrement;

	private bool overSpeed; //Ensures speeding violation cannot repeat while currently speeding (ie: getting 100 speeding violations when speeding for 2 seconds)
	private int speedLimit; //Derived from the ViolationManager

	private bool useMPH;

	private float highSpeed;

	private ViolationManager violations;
	private GlobalScript globalScript;

	void Start()
	{
		globalScript = DS.FindGlobalScript ();
		violations = globalScript.GetComponent<ViolationManager> ();
		car = transform.root.GetComponent<Rigidbody> ();

		speedLimit = globalScript.useMPH ? violations.m_SpeedLimit + 5 : violations.m_SpeedLimit + 8;						// Speed is only over limit when it's 5 mph over

		// If autonomous car exists
		if(transform.root.GetComponent<CarAIControl>())
			transform.root.GetComponent<CarAIControl>().startingSpeed = globalScript.useMPH ? speedLimit - 5 : speedLimit - 8;	// This determines the speed the autonomous car drives. It will drive the speed limit rather than 5mph over
		
        overSpeed = false;

		if (!globalScript.useMPH)
			m_SpeedType = SpeedType.KPH;

		SetNeedleAngles ();
		SetFace ();
	}

	void Update ()
	{
		switch (m_SpeedType) {

		case SpeedType.MPH:
			carSpeed = car.velocity.magnitude * 2.237f;

			break;

		case SpeedType.KPH:
			carSpeed = car.velocity.magnitude * 3.6f;

			break;
		}
			
		switch (m_FaceType) {

		case FaceType.Analog:
			
			if (carSpeed <= maxSpeed + 1) {
				// Angle uses MPH even if speed should be in KPH so the the needle still moves properly (Only the speedometer face changes)
				angle = (car.velocity.magnitude * 2.237f * scaledIncrement) + minAngle;
				needle.transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
			}

			break;

		case FaceType.Digital:
			digitalText.text = Mathf.Round(carSpeed).ToString();

			break;

		}
			
		//////////////////////////////SPEED CHECK////////////////////////////////
		//If not previously speeding
		if(!overSpeed){
			//and now speeding
			if(carSpeed > speedLimit)
			{
				overSpeed = true; //Speeding state is true and disable violation detection
				violations.speedViolation(); //get a violation
			}
		}else{
			if(carSpeed > highSpeed)
				highSpeed = carSpeed;

			//If previous speeding, and now no longer speeding
			if(carSpeed < speedLimit)
			{
				violations.logSpeed(Mathf.Round (highSpeed));
				overSpeed = false; //Speeding state is false and now able to be detected for speeding again
			}
		}
		///////////////////////////SPEED CHECK END//////////////////////////////
	}

	void SetFace()
	{
		// Determine face type
		if (globalScript.m_DigitalSpeedometer)
			m_FaceType = FaceType.Digital;

		switch (m_FaceType) {

		case FaceType.Analog:

			changeFaceToAnalog ();
			break;

		case FaceType.Digital:

			changeFaceToDigital ();
			break;

		}
	}

	void SetNeedleAngles()
	{
		//55 degrees points the needle to 0. 149 degrees points to 70 (Numbers may have been changed, but the concept is the same)
		minAngle = 235;
		maxAngle = 481;

		//When the car hits its peak speed (in this case 70), that will determine the maxSpeed (Numbers may have been changed, but the concept is the same)
		minSpeed = 0;
		maxSpeed = 180;

		//Takes the differences from min and max numbers
		angleDiff = maxAngle - minAngle;
		speedDiff = maxSpeed - minSpeed;

		//Scales the numbers down so that the angle and speed can be synced
		//This number means for every 1 mph that is reached, this number is used instead
		scaledIncrement = angleDiff/speedDiff;

		zeroAngle = minAngle; //Angles 55 to 149 are speed ranges from 0 - 70mph
		angle = zeroAngle; //Start position of angle
		//car = transform.parent.gameObject;
	}

	void changeFaceToAnalog()
	{
		m_FaceType = FaceType.Analog;

		needle.SetActive (true);

		if (m_SpeedType == SpeedType.MPH)
			face.GetComponent<Renderer> ().material = MPH_Face;
		else
			face.GetComponent<Renderer> ().material = KPH_Face;
		
		digitalText.gameObject.SetActive (false);
	}

	void changeFaceToDigital()
	{
		m_FaceType = FaceType.Digital;

		needle.SetActive (false);
		face.GetComponent<Renderer> ().material = Digital_Face;
		digitalText.gameObject.SetActive (true);
	}

	/*
	NOTES:
	94 //difference for angles
	70 //difference for magnitude

	1.34 //70 goes into 94 this many times. So every mph increment must increment by 1.34 degrees

	//0 = 55
	//1 = 1.34
	//2 = 2.68
	//3 = 4.02

	*/
}